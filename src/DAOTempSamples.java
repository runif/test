package persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author runef
 */
public class DAOTempSamples extends DbConnection {

    public static final String PREFIX = "jdbc:sqlite:";
    public static final String DRIVER_NAME = "org.sqlite.JDBC";
    private final String fSampleTableName;
    private final String fSchemaSampleTable;
    private PreparedStatement pInsStmt = null;

    public DAOTempSamples(Properties props) throws SQLException, ClassNotFoundException {
        super(DRIVER_NAME, PREFIX + props.getProperty("url"), props);
        super.getConnection().setAutoCommit(false);
        this.fSampleTableName = props.getProperty("SampleTableName");
        this.fSchemaSampleTable = props.getProperty("SchemaSampleTable");
        super.executeStatement(fSchemaSampleTable);
        super.getConnection().commit();
    }

    public ResultSet getSamples(String pk, List<String> columns) throws SQLException {
        String sql = "SELECT " + columns.toString().substring(1, columns.toString().length() - 1)
                + " FROM " + this.fSampleTableName;
        ResultSet rs = super.executeQuery(sql);
        return rs;
    }

    public boolean addSamples(List<List<String>> samples, List<String> colNames) throws SQLException {
        String sqlDrop = "DROP TABLE IF EXISTS " + this.fSampleTableName;
        super.executeStatement(sqlDrop);
        super.getConnection().commit();

        super.executeStatement(this.fSchemaSampleTable);
        super.getConnection().commit();

        if (!samples.isEmpty()) {

            String sql = prepareInsertStatement(colNames, fSampleTableName);

            pInsStmt = super.getConnection().prepareStatement(sql);

            for (List<String> row : samples) {

                int k = 1;
                for (String col : row) {

                    pInsStmt.setString(k++, col);
                }
                pInsStmt.addBatch();
            }
            pInsStmt.executeBatch();
            super.getConnection().commit();
            pInsStmt.close();
            return true;
        }

        return false;
    }
    
    public void vacuum() {
        try {
            
            
            super.getConnection().close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOTempSamples.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Creates a prepared INSERT statement with place holders. The parameters
     * must be passed to this method in the same sequence as the column names.
     *
     * @param columnNames - names of the columns to be inserted
     * @param tableName - name of table to be updated
     * @return - the prepared statement ready to be parameterized
     * @throws SQLException - if the SQL syntax is not adhered to
     */
    private String prepareInsertStatement(List<String> columnNames,
            String tableName) throws SQLException {

        StringBuilder sql1 = new StringBuilder("INSERT INTO ").append(tableName).append("(");
        StringBuilder sql2 = new StringBuilder("VALUES(");
        for (int i = 0; i < columnNames.size(); i++) {
            if (i < columnNames.size() - 1) {
                sql1.append(columnNames.get(i)).append(",");
                sql2.append("?,");
            } else {
                sql1.append(columnNames.get(i)).append(")");
                sql2.append("?)");
            }
        }
        sql1.append(sql2);
        return sql1.toString();
    }
}
