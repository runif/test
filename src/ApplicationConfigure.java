package business;

import business.ApplicationBusiness;
import persistence.SignalCollection;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author runef
 */
public class ApplicationConfigure {

    private XmlFileReader xmlFile;
    private String confFileName = "config.xml";

    public ApplicationConfigure() throws SAXException, IOException, ParserConfigurationException {
        xmlFile = new XmlFileReader(this.confFileName);
    }
    
    public void configureApp() {
        initSignals();
        setUrlLogger();
        setSignalTableName();
        setMaxNoOfRows();
        setUrlFiltered();
        setMaxFileSizeFiltered();
        setFilterInterval();
    }

    public void initSignals() {
        List<List<String>> sig = this.xmlFile.getSignalParams();
        SignalCollection sigCol = SignalCollection.getUniqueInstance();
        sig.forEach((signal) -> {
            sigCol.addSignal(signal.get(0), signal.get(1), signal.get(2), signal.get(3), 
                    signal.get(4), Arrays.asList(signal.get(5).split(",")), signal.get(6));
        });
    }

    public void setUrlLogger() {
//        ApplicationBusiness.getUniqueInstance().setDbLoggerUrl(conf.getUrlLogger());
    }

    public void setSignalTableName() {
        ApplicationBusiness.getUniqueInstance().setSignalTableName(xmlFile.getSignalTableName());
    }

    public void setMaxNoOfRows() {
        ApplicationBusiness.getUniqueInstance().setMaxNoOfRows(xmlFile.getMaxNoOfRows());
    }

    public void setUrlFiltered() {
//        ApplicationBusiness.getUniqueInstance().setDbFilteredUrl(conf.getUrlFiltered());
    }
    
    public void setMaxFileSizeFiltered() {
//        ApplicationBusiness.getUniqueInstance().setMaxFileSizeFiltered(Long.parseLong(conf.getMaxFileSizeFiltered()));
    }
    
    public void setFilterInterval() {
        ApplicationBusiness.getUniqueInstance().setFilterInterval(Integer.parseInt(xmlFile.getFilterInterval()));
    }
    
    public String getFilterFromStart() {
        return this.xmlFile.getFilterFromStart();
    }
    
    public String getStartWithUi() {
        return this.xmlFile.getStartWithUi();
    }
    
    public String getFilterContinuously() {
        return this.xmlFile.getFilterContinuously();
    }

}
