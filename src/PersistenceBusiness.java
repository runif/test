package persistence;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author runef
 */
public class PersistenceBusiness {

    private static volatile PersistenceBusiness fInstance;
    
    private ExecutorService threadPool;
    
    private static final FilterData filterData = new FilterData();

    private PersistenceBusiness() {
        this.startPersistenceLayer();
    }
    
    public void startPersistenceLayer() {
        threadPool = Executors.newFixedThreadPool(5);
    }
    
    public void stopPersistenceLayer() {
        threadPool.shutdown();
    }
    
    public void startFiltering() {
        filterData.setShouldFilter(true);
        threadPool.execute(filterData);
    }
    
    public void stopFiltering() {
        filterData.setShouldFilter(false);
    }
    
    public static PersistenceBusiness getInstance() {
        PersistenceBusiness instance = PersistenceBusiness.fInstance;
        if (instance == null) {
            synchronized(PersistenceBusiness.class) {
                instance = PersistenceBusiness.fInstance;
                if (instance == null) {
                    PersistenceBusiness.fInstance = instance = new PersistenceBusiness();
                }
            }
        }
        return instance;
    }
}
