
package persistence;

import java.util.List;
import java.util.ListIterator;
import persistence.SignalCollection.DbSignal;

/**
 *
 * @author runef
 */
public class DataFilter {

    private boolean firstScan = true;
    private SignalCollection signals;
    
    private static volatile DataFilter fInstance;
    
    private DataFilter() {
        this.signals = SignalCollection.getUniqueInstance();
    }
    
    public static DataFilter getInstance() {
        DataFilter instance = DataFilter.fInstance;
        if (instance == null) {
            synchronized(DataFilter.class){
                instance = DataFilter.fInstance;
                if(instance == null) {
                    DataFilter.fInstance = instance = new DataFilter();
                }
            }
        }
        return instance;
    }
    
    /**
     *
     * @param samples
     * @param colNames
     * @return
     */
    public List<List<String>> removeUnLogables(List<List<String>> samples, List<String> colNames) {
        DbSignal sig;
        String currVal;
        boolean triggerFound;
        int colNo;
        List<String> row;
        String col;
        ListIterator<List<String>> itRow = samples.listIterator();

        while (itRow.hasNext()) {
            row = itRow.next();
            ListIterator<String> itCol = row.listIterator();
            triggerFound = false;
            while (!triggerFound && itCol.hasNext()) {
                colNo = itCol.nextIndex();
                col = itCol.next();
                sig = this.signals.getSignal(colNames.get(colNo));
                
                if (sig != null && sig.isTrigger(col)) {
                    triggerFound = true;
                }
            }
            if (!triggerFound) {
                itRow.remove();
            }
        }
        // If the result list is not empty, filter the remaining data
        // The first row will alwyays be kept with all values
        if (!samples.isEmpty()) {
            for (List<String> element : samples) {
                for (int i = 0; i < element.size(); i++) {
                    // The first row of the result will not be filtered, 
                    // i.e. all values will be written to the filtered database
                    sig = signals.getSignal(colNames.get(i));
                    currVal = element.get(i);

                    // If this is the first filtering cycle since app start, let
                    // all values pass thru un-filtered
                    if (firstScan) {
                        sig.setValue(currVal);

                        // Check if the current value is a new value. If it is a 
                        // new value, update the signal object and leave the value 
                        // unchanged in the table. If not new value, set the table 
                        // value to null
                    } else if (sig.hasNewValue(currVal)) {
                        sig.setValue(currVal);
                    } else {
                        element.set(i, null);
                    }

                }
                if (firstScan == true) {
                    firstScan = false;
                }
            }
        }
        return samples;
    }
    
}
