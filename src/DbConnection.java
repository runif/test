package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.util.Properties;

/**
 * This class will set a connection to a database and handle all statements and
 * queries to the database. It will only take statements and queries as
 * parameters to its executing methods. It will not keep any statements or
 * queries. This class does not provide any protection against faulty SQL
 * statements. Faulty statements and queries passed to the database will return
 * an exception which is passed to the calling instance.
 *
 * @author runef
 */
public abstract class DbConnection {

    private String fUrl = "";
    private Connection fConn = null;
    private Statement fStmt = null;

    /**
     * This constructor will establish a connection to the database on the
     * specified url. When the object is successfully instantiated it is ready
     * to pass statements to the database.
     *
     * @param driverName
     * @param url
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    public DbConnection(String driverName, String url) throws SQLException, ClassNotFoundException {
        this.setDriver(driverName);
        this.setUrl(url);
        this.fConn = DriverManager.getConnection(this.getUrl());
        this.createStatement();
    }

    /**
     * This constructor also accepts a properties object to set initial connection 
     * settings.
     * @param driver
     * @param url
     * @param props
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public DbConnection(String driver, String url, Properties props) throws SQLException, ClassNotFoundException {
        this.setDriver(driver);
        this.setUrl(url);
        this.fConn = DriverManager.getConnection(this.getUrl(), props);
        this.createStatement();
    }

    private void setDriver(String driverName) throws ClassNotFoundException {
        Class.forName(driverName);
    }

    private Statement createStatement() throws SQLException {
        return this.fStmt = this.fConn.createStatement();
    }

    /**
     * Return the connection of this object
     *
     * @return
     */
    public Connection getConnection() {
        return this.fConn;
    }

    /**
     * Return the statement of this object.
     *
     * @return
     */
    public Statement getStatement() {
        return this.fStmt;
    }

    /**
     * Execute an SQL statement of any type. The statement may return many
     * different types of results. This method only returns true or false. True
     * if the executed statament returns a ResultSet and false otherwise.
     *
     * @param sql
     * @return
     * @throws SQLException
     * @throws java.sql.SQLTimeoutException
     */
    public boolean executeStatement(String sql) throws SQLException, SQLTimeoutException {
        if (this.fStmt.isClosed()) {
            this.fStmt = createStatement();
        }
        return this.fStmt.execute(sql);
    }

    /**
     *
     * @param sql
     * @return
     * @throws SQLException
     * @throws java.sql.SQLTimeoutException
     */
    public ResultSet executeQuery(String sql) throws SQLException, SQLTimeoutException {
        if (this.fStmt.isClosed()) {
            this.fStmt = createStatement();
        }
        return this.fStmt.executeQuery(sql);
    }

    /**
     *
     * @param sql
     * @return
     * @throws SQLException
     * @throws java.sql.SQLTimeoutException
     */
    public int executeUpdate(String sql) throws SQLException, SQLTimeoutException {
        if (this.fStmt.isClosed()) {
            this.fStmt = createStatement();
        }
        return this.fStmt.executeUpdate(sql);
    }

    /**
     *
     * @throws SQLException
     */
    public void closeConnection() throws SQLException {
        this.fConn.close();
    }

    /**
     *
     * @throws SQLException
     */
    public void closeStatement() throws SQLException {
        this.fStmt.close();
    }

    /**
     * @return the fUrl
     */
    public String getUrl() {
        return fUrl;
    }

    /**
     * @param fUrl the fUrl to set
     */
    public void setUrl(String fUrl) {
        this.fUrl = fUrl;
    }

}
