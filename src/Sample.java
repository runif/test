package persistence;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author runef
 */
public class Sample {

    private String timestamp;
    private List<Signal> signals = new ArrayList<>();

    private static final Map<String, Integer> types = new HashMap<>();

    public Sample(String timestamp) {
        this.timestamp = timestamp;
        types.put("double", 1);
        types.put("integer", 2);
        types.put("boolean", 3);
    }

    public void addSignal(Signal signal) {
        this.signals.add(signal);
    }
    
    public double getTimestamp() {
        return Double.parseDouble(timestamp);
    }

    public byte[] sampleToBlob() throws IOException {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);

        int caseOf = 0;
        for (Signal sig : signals) {

            dos.writeShort(Integer.parseInt(sig.getId()));

            caseOf = types.get(sig.getType().toLowerCase());

            switch (caseOf) {
                case 1:
                    dos.writeDouble(Double.parseDouble(sig.getValue()));
                    break;

                case 2:
                    dos.writeInt(Integer.parseInt(sig.getValue()));
                    break;

                case 3:
                    dos.writeBoolean(Boolean.parseBoolean(sig.getValue()));
                    break;
            }
        }
        dos.flush();
        
        byte[] data = bos.toByteArray();
        short len = (short) (data.length + 2);
        
        byte[] blob = new byte[data.length + 2];
        blob[1] = (byte) (len);
        blob[0] = (byte) (len >> 8);
        
        for (int i = 0; i < data.length; i++) {
            blob[i + 2] = data[i];
        }

        return blob;
    }
}
