package persistence;

import business.PropertiesFileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author runef
 */
public class ApplicationPersistence {

    private static volatile ApplicationPersistence appPersistence;

    private DAOFilteredSamples fFilteredDB;
    private DAORawSamples fLoggerDB;

    private ApplicationPersistence() {
        this.init();
    }

    public void insertFilteredSamples(List<List<String>> samples, List<String> colNames) {
//        try {
//            this.fFilteredDB.addSamples(samples, colNames);
//        } catch (SQLException ex) {
//            Logger.getLogger(ApplicationPersistence.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    public List<List<String>> getFilteredSamples(String pk, List<String> columns) {
        List<List<String>> samples = new ArrayList<>();
        try {
            samples = this.fFilteredDB.getSamples(pk, columns);
        } catch (SQLException ex) {
            Logger.getLogger(ApplicationPersistence.class.getName()).log(Level.SEVERE, null, ex);
        }
        return samples;
    }

    public List<List<String>> getFilteredSample(String timestamp, List<String> columns) {
        List<List<String>> retSamples = new ArrayList<>();
        try {
            retSamples = fFilteredDB.getSample(timestamp, columns);
        } catch (SQLException ex) {
            Logger.getLogger(ApplicationPersistence.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retSamples;
    }

    public String getLargestPkFilteredSamples() {
        try {
            return fFilteredDB.getMaxTimestamp();
        } catch (SQLException ex) {
            Logger.getLogger(ApplicationPersistence.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    /**
     * Returns samples in a the bulk size defined in the properties file for the
     * database if the quantity is available. Otherwise, returns all avaliable.
     *
     * @param pk - primary key of sample table
     * @param columns - column names to get
     * @return - 2D table of the samples where the first element is the column
     * names in correct order
     */
    public ResultSet getRawSamples(String pk, List<String> columns) {
        try {
            return this.fLoggerDB.getSamples(pk, columns);
            
        } catch (SQLException ex) {
            Logger.getLogger(ApplicationPersistence.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public List<String> getKeysFilteredSamples(String timestamp) {
        return fFilteredDB.getKeyTableTimestamps(timestamp);
    }

    public String getLargestPkRawSamples() {
        try {
            return this.fLoggerDB.getPkMaxValue();
        } catch (SQLException ex) {
            Logger.getLogger(ApplicationPersistence.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public static ApplicationPersistence getUniqueInstance() {
        ApplicationPersistence instance = ApplicationPersistence.appPersistence;
        if (instance == null) {
            synchronized (ApplicationPersistence.class) {
                instance = ApplicationPersistence.appPersistence;
                if (ApplicationPersistence.appPersistence == null) {
                    ApplicationPersistence.appPersistence = instance = new ApplicationPersistence();
                }
            }
        }
        return instance;
    }

    private void init() {
//        try {
//            this.fFilteredDB = new DAOFilteredSamples(PropertiesFileReader.getProps("sqlite-filtered.properties"));
//            this.fLoggerDB = new DAORawSamples(PropertiesFileReader.getProps("sqlite-logger.properties"));
//        } catch (IOException | SQLException | ClassNotFoundException ex) {
//            Logger.getLogger(ApplicationPersistence.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }
}
