package persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This class class holds a collection of signals. It has an inner class of
 * signal which may only be instantiated thru this enclosing class. Any signal
 * must be part this collection.
 *
 * @author runef
 */
public class SignalCollection {

    private List<DbSignal> signals = new ArrayList<>();
    private volatile static SignalCollection uniqueInstance;

    private SignalCollection() {
    }

    /**
     * Returns a unique instance of this class. The method is synchronized and
     * thread-safe.
     *
     * @return - a unique instance of this class
     */
    public static SignalCollection getUniqueInstance() {
        SignalCollection instance = SignalCollection.uniqueInstance;
        if (instance == null) {
            synchronized (SignalCollection.class) {
                instance = SignalCollection.uniqueInstance;
                if (SignalCollection.uniqueInstance == null) {
                    SignalCollection.uniqueInstance = instance = new SignalCollection();
                }
            }
        }
        return instance;
    }

    /**
     * Adds a new signal to this collection. The fValue and sensitivty must be
     * numbers, null is not accepted.
     *
     * @param name - name of the signal. Case insensitive
     * @param dataType - database specific datatype
     * @param value - the current or default fValue. Must be a number
     * @param sensitivity - the desired fSensitivity between 0 and 1
     * @param primaryKey - if the signal is primary key set to 1 else 0
     * @param trig
     * @param id
     * @return - true if the object was successfully added, false otherwise
     */
    public boolean addSignal(String name, String dataType, String value, String sensitivity,
            String primaryKey, List<String> trig, String id) throws NumberFormatException {
        // Check that the list doesn't contain the object already
        if (!this.signals.stream().noneMatch((signal) -> (signal.getName().equalsIgnoreCase(name)))) {
            return false;
        }
        return signals.add(new DbSignal(name, dataType, value, sensitivity, primaryKey, trig, id));
    }

    public List<DbSignal> getSignals() {
        return this.signals;
    }

    /**
     * Returns the signal that is the primary key of the table.
     *
     * @return - signal that is primary key or null if no signal is found
     */
    public DbSignal getPrimaryKeySignal() {
        for (DbSignal signal : this.signals) {
            if (signal.getPrimaryKey().equals("1")) {
                return this.getSignal(signal.getName());
            }
        }
        return null;
    }

    /**
     * Remove a signal from this collection. Case insensitive
     *
     * @param name - fName of signal to remove. Case insensitive
     * @return - true if success. False otherwise
     */
    public boolean removeSignal(String name) {
        for (DbSignal signal : this.signals) {
            if (signal.getName().equalsIgnoreCase(name)) {
                return this.signals.remove(signal);
            }
        }
        return false;
    }

    /**
     * Returns the signal if this collection has it. The method will return null
     * if the signal does not exist.
     *
     * @param name - fName of signal to return. Case insensitive
     * @return - signal object or null if not found
     */
    public DbSignal getSignal(String name) {
        for (DbSignal signal : signals) {
            if (signal.getName().equalsIgnoreCase(name)) {
                return signal;
            }
        }
        return null;
    }

    /**
     * Resturn an itarator of this collection
     *
     * @return - iterator of this collection
     */
    public Iterator getIterator() {
        return this.signals.iterator();
    }

    /**
     * Inner class of signal. Only to be instantiated thru the enclosing class.
     * No signal object should live outside of the SignalCollection class.
     * Therefore the constructor is private.
     */
    public class DbSignal {

        private String fName;
        private String fDataType;
        private String fValue;
        private String fSensitivity;
        private String fPrimaryKey;
        private double fSens;
        private double fVal;
        private String id;

        private List<String> fTriggers = new ArrayList<>();

        private DbSignal(String name, String dataType, String value, String sensitivity,
                String primaryKey, List<String> trig, String id) throws NumberFormatException {
            this.id = id;
            this.fName = name;
            this.fDataType = dataType;
            this.fPrimaryKey = primaryKey;
            if (trig != null) {
                this.fTriggers.addAll(trig);
            }
            setValue(value);
            setSensitivity(sensitivity);
        }

        /**
         * Checks against the set fSensitivity whether or not the fValue passed
         * to this method represents a greater change than the set fSensitivity.
         * The fSensitivity has an exponential decay for increasing values of
         * the input.
         *
         * @param value A new fValue to be compared with the last fValue written
         * to this object. Value must be a number! Decimals must use "." (dot)
         * NOT "," (comma)
         * @return - true if the change in fValue is greater than the set
         * fSensitivity, else false
         * @throws NumberFormatException
         */
        public boolean hasNewValue(String value) throws NumberFormatException {

            if (value != null) {
                if (Math.abs(Math.abs(this.fVal) - Math.abs(Double.parseDouble(value))) > this.fSens) {
                    return true;
                }
            }
            return false;
        }

        /**
         * Check if the signal has a trigger that equals the value passed to
         * this method. The method is NOT case sensitive.
         *
         * @param val - value to be compared
         * @return - true if trigger found, false otherwise
         */
        public boolean isTrigger(String val) {
            boolean triggerFound = false;
            if (!this.fTriggers.isEmpty()) {
                for (String trig : this.fTriggers) {
                    if (trig.equalsIgnoreCase(val)) {
                        triggerFound = true;
                    }
                }
            }
            return triggerFound;
        }

        /**
         * @return the fName
         */
        public String getName() {
            return fName;
        }

        /**
         * @param name the fName to set
         */
        public void setName(String name) {
            this.fName = name;
        }

        /**
         * @return the fDataType
         */
        public String getDataType() {
            return fDataType;
        }

        /**
         * @param dataType the fDataType to set
         */
        public void setDataType(String dataType) {
            this.fDataType = dataType;
        }

        /**
         * @return the fValue
         */
        public String getValue() {
            return fValue;
        }

        /**
         * @param value the fValue to set
         * @throws NumberFormatException
         */
        public void setValue(String value) throws NumberFormatException {

            if (value != null) {
                this.fValue = value;
                this.fVal = Double.parseDouble(value);
            }
        }

        /**
         * @return the fSensitivity
         */
        public String getSensitivity() {
            return fSensitivity;
        }

        /**
         * @param sensitivity the fSensitivity to set
         * @throws NumberFormatException
         */
        public void setSensitivity(String sensitivity) throws NumberFormatException {

            if (sensitivity != null) {
                this.fSensitivity = sensitivity;
                this.fSens = Double.parseDouble(sensitivity);
            }
        }

        /**
         * @return the fPrimaryKey
         */
        public String getPrimaryKey() {
            return fPrimaryKey;
        }

        /**
         * @param primaryKey the fPrimaryKey to set
         */
        public void setPrimaryKey(String primaryKey) {
            this.fPrimaryKey = primaryKey;
        }
        
        public List<String> getTriggers() {
            return this.fTriggers;
        }

        /**
         * @return the id
         */
        public String getId() {
            return id;
        }

        /**
         * @param id the id to set
         */
        public void setId(String id) {
            this.id = id;
        }

    }

}
