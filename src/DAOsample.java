package persistence;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author runef
 * @param <T>
 */
public interface DAOsample {
    
    public List<List<String>> getSamples(String pk, List<String> colNames) throws SQLException;
    
    public void addSamples(List<List<String>> samples, List<String> colNames) throws SQLException;
    
}
