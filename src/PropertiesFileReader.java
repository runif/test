
package business;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author runef
 */
public class PropertiesFileReader {
    
    public static Properties getProps(String fileUrl) throws FileNotFoundException, IOException {
        FileReader reader = new FileReader(fileUrl);
        Properties props = new Properties();
        props.load(reader);
        return props;
    }
    
}
