
package persistence;

import business.PropertiesFileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author runef
 */
public class FilterData implements Runnable {

    private DAOFilteredSamples fFilteredDB;
    private DAORawSamples fLoggerDB;
    private DataFilter fDataFilter;
    private Long fLastRetrieved = 0L;
    private volatile boolean fShouldRun = true;
    private int fBulkSize = 10000;
    private List<String> columnsRaw;

    public FilterData() {
        this.fDataFilter = DataFilter.getInstance();
        this.init();
    }

    @Override
    public void run() {
        List<List<String>> rsList;

        boolean shouldWait = false;

        do {
            synchronized (this) {
                try {
                    // Select all rows from the logger where the primary key is
                    // greater than the last one written to the filtered database

                    rsList = this.fLoggerDB.getSampleBulk(fLastRetrieved.toString(), columnsRaw);

                    List<String> colNames = rsList.remove(0);
                    List<String> types = rsList.remove(0);
                    
                    fLastRetrieved += rsList.size();

                    // If bulk size was not reached, take at breake
                    shouldWait = !(rsList.size() < fBulkSize);

                    // Check for new values and update result set list
                    rsList = this.fDataFilter.removeUnLogables(rsList, colNames);

                    // Check if the list contains any data
                    if (!rsList.isEmpty()) {
                        this.fFilteredDB.addSamples(rsList, colNames, types);
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(FilterData.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (!shouldWait) {
                    try {
                        wait(60000); // Wait for 60 seconds if samples bulk was not filled
                    } catch (InterruptedException ex) {
                        Logger.getLogger(FilterData.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        } while (shouldFilter());
    }

    private void init() {
        try {
            this.fFilteredDB = new DAOFilteredSamples(PropertiesFileReader.getProps("sqlite-filtered.properties"));
            this.fLoggerDB = new DAORawSamples(PropertiesFileReader.getProps("sqlite-logger.properties"));
            this.columnsRaw = fLoggerDB.getColumns();
        } catch (IOException | SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ApplicationPersistence.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the fShouldRun
     */
    public boolean shouldFilter() {
        return fShouldRun;
    }

    /**
     * @param fShouldRun the fShouldRun to set
     */
    public void setShouldFilter(boolean fShouldRun) {
        this.fShouldRun = fShouldRun;
    }

}
