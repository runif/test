package persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import business.TrivialMethods;
import java.io.IOException;
import persistence.SignalCollection.DbSignal;

/**
 * This is a data access object created specifically for the filterted database.
 * It provides access for all the available data on the filtered database. It's
 * properties are provided thru an external .properties file.
 *
 * @author runef
 */
public class DAOFilteredSamples extends DbConnection {

    private String fSampleTableName;
    private String fSignalMapname;
    private String fKeyTableName;
    private List<String> schemas = new ArrayList<>();

    private PreparedStatement pInsStmt = null;
    private int fBulkSize;
    private String fTimestampName;
    private int samplesAdded = 0;
    private List<String> lastValidSample = new ArrayList<>();

    private SignalCollection signals = SignalCollection.getUniqueInstance();

    private static final String PREFIX = "jdbc:sqlite:";
    private static final String DRIVER_NAME = "org.sqlite.JDBC";
    private static final long ID = System.currentTimeMillis();

    public DAOFilteredSamples(Properties props) throws SQLException, ClassNotFoundException {
        super(DRIVER_NAME, PREFIX + props.getProperty("url") + ID + "BLOB.db", props);
        super.getConnection().setAutoCommit(false);
        this.fSignalMapname = props.getProperty("SignalMapName");
        this.fSampleTableName = props.getProperty("SampleTableName");
        this.fBulkSize = Integer.parseInt(props.getProperty("BulkSize"));
        this.fTimestampName = props.getProperty("TimestampColName");
        this.fKeyTableName = props.getProperty("KeyTableName");
        schemas.add(props.getProperty("SchemaSampleTable"));
        schemas.add(props.getProperty("SchemaSignalMap"));
        schemas.add(props.getProperty("SchemaKeyTable"));
        this.setupDatabase(schemas);
        this.populateKeysTable();
    }

    private void populateKeysTable() {
        PreparedStatement pstmt = null;
        try {
            String sql = this.prepareSampleInsertStatement(this.getColumnNamesKey(), this.fSignalMapname);
            pstmt = super.getConnection().prepareStatement(sql);
            List<DbSignal> sig = signals.getSignals();
            for (int i = 1; i < sig.size(); i++) {
                pstmt.setString(1, sig.get(i).getId());
                pstmt.setString(2, sig.get(i).getName());
                pstmt.setString(3, sig.get(i).getDataType());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            super.getConnection().commit();
        } catch (SQLException ex) {
            Logger.getLogger(DAOFilteredSamples.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DAOFilteredSamples.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     *
     * @param samples
     * @param colNames
     * @param types
     * @throws SQLException
     */
    public void addSamples(List<List<String>> samples, List<String> colNames, List<String> types) throws SQLException {
        if (!samples.isEmpty()) {

            if (lastValidSample.isEmpty()) {
                lastValidSample.addAll(samples.get(0));
                addEntryPoint(lastValidSample);
            }

            String sql = prepareSampleInsertStatement(this.getColumnNamesSample(), fSampleTableName);

            if (pInsStmt == null) {
                pInsStmt = super.getConnection().prepareStatement(sql);
            }

            for (List<String> row : samples) {

                int i = 0;
                for (String col : row) {

                    if (col != null) {
                        lastValidSample.set(i, col);
                    }
                    i++;
                }

                if (samplesAdded >= this.fBulkSize) {

                    for (int h = 0; h < row.size(); h++) {
                        
                        row.set(h, lastValidSample.get(h));
                    }

                    this.addEntryPoint(row);

                    samplesAdded = 0;
                }
                samplesAdded++;
            }

            SampleCollection blobs = this.toBlobs(samples, colNames, types);

            for (Sample sample : blobs.getSamples()) {
                try {
                    pInsStmt.setDouble(1, sample.getTimestamp());
                    pInsStmt.setBytes(2, sample.sampleToBlob());
                } catch (IOException ex) {
                    Logger.getLogger(DAOFilteredSamples.class.getName()).log(Level.SEVERE, null, ex);
                }
                pInsStmt.addBatch();
            }
            pInsStmt.executeBatch();
            super.getConnection().commit();
            blobs.clearSamples();
        }
    }

    /**
     *
     * @param timestamp
     * @param columns
     * @return
     * @throws SQLException
     */
    public List<List<String>> getSamples(String timestamp, List<String> columns)
            throws SQLException {
        List<String> keys = this.getKeyTableTimestamps(timestamp);
        String sql = "SELECT " + columns.toString().substring(1, columns.toString().length() - 1)
                + " FROM " + this.fSampleTableName + " WHERE " + this.fTimestampName + " >= "
                + keys.get(keys.size() - 1) + " LIMIT " + this.fBulkSize;
        ResultSet rs = super.executeQuery(sql);
        return TrivialMethods.populateTable(TrivialMethods.resultSetToArray(rs, true, true));
    }

    public List<List<String>> getSamples(String timestampStart, String timestampStop, List<String> columns)
            throws SQLException {
        List<String> keys = this.getKeyTableTimestamps(timestampStart);
        String sql = "SELECT " + columns.toString().substring(1, columns.toString().length() - 1)
                + " FROM " + this.fSampleTableName + " WHERE " + this.fTimestampName + " BETWEEN "
                + keys.get(keys.size() - 1) + " AND " + timestampStop;
        ResultSet rs = super.executeQuery(sql);
        return TrivialMethods.populateTable(TrivialMethods.resultSetToArray(rs, true, false));
    }

    public List<List<String>> getSample(String timestamp, List<String> colNames) throws SQLException {

        List<List<String>> samples = this.getSamples(timestamp, colNames);
        List<String> columns = samples.remove(0);
        double tstmp = Double.parseDouble(timestamp);
        List<List<String>> returnSample = new ArrayList<>();
        int i = 0;
        for (List<String> row : samples) {
            if (Double.parseDouble(row.get(0)) > tstmp) {
                returnSample.add(columns);
                returnSample.add(samples.get(i - 1));
                break;
            } else if (Double.parseDouble(row.get(0)) == tstmp) {
                returnSample.add(columns);
                returnSample.add(samples.get(i));
                break;
            }
            i++;
        }
        return returnSample;
    }

    /**
     *
     * @param timestamp
     * @return
     */
    public List<String> getKeyTableTimestamps(String timestamp) {
        List<String> table = new ArrayList<>();
        String sql = "SELECT " + this.fTimestampName + " FROM " + this.fSignalMapname
                + " WHERE timestamp <= " + timestamp;

        try {
            ResultSet rs = super.executeQuery(sql);
            while (rs.next()) {
                table.add(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOFilteredSamples.class.getName()).log(Level.SEVERE, null, ex);
        }
        return table;
    }

    /**
     * Returns the largest timestamp of the sample table
     *
     * @return - largest timestamp
     * @throws SQLException
     */
    public String getMaxTimestamp() throws SQLException {
        ResultSet rs = super.executeQuery("SELECT MAX(" + this.fTimestampName + ")" + " FROM " + this.fSampleTableName);
        return rs.getString("MAX(" + this.fTimestampName + ")");
    }

    public List<String> getColumnNamesSample() throws SQLException {
        List<String> columns;
        String sql = "SELECT * FROM " + this.fSampleTableName + " LIMIT 0";
        ResultSet rs = super.executeQuery(sql);
        columns = TrivialMethods.resultSetToArray(rs, true, false).get(0);
        return columns;
    }

    public List<String> getColumnNamesKey() throws SQLException {
        List<String> columns;
        String sql = "SELECT * FROM " + this.fSignalMapname + " LIMIT 0";
        ResultSet rs = super.executeQuery(sql);
        columns = TrivialMethods.resultSetToArray(rs, true, false).get(0);
        return columns;
    }

    /**
     *
     * @param sample
     * @throws SQLException
     */
    private void addEntryPoint(List<String> sample) throws SQLException {
        super.executeStatement("INSERT INTO " + this.fKeyTableName + "(" + this.fTimestampName + ") VALUES(" + sample.get(0) + ")");
        super.getConnection().commit();
    }

    /**
     * Creates a prepared INSERT statement with place holders. The parameters
     * must be passed to this method in the same sequence as the column names.
     *
     * @param columnNames - names of the columns to be inserted
     * @param tableName - name of table to be updated
     * @return - the prepared statement ready to be parameterized
     * @throws SQLException - if the SQL syntax is not adhered to
     */
    private String prepareSampleInsertStatement(List<String> columnNames,
            String tableName) throws SQLException {

        StringBuilder sql1 = new StringBuilder("INSERT INTO ").append(tableName).append("(");
        StringBuilder sql2 = new StringBuilder("VALUES(");
        for (int i = 0; i < columnNames.size(); i++) {
            if (i < columnNames.size() - 1) {
                sql1.append(columnNames.get(i)).append(",");
                sql2.append("?,");
            } else {
                sql1.append(columnNames.get(i)).append(")");
                sql2.append("?)");
            }
        }
        sql1.append(sql2);
        return sql1.toString();
    }

    private List<String> sortResultSet(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();

        List<String> logSchema = new ArrayList<>();

        int noOfCol = rsmd.getColumnCount();

        while (rs.next()) {
            for (int i = 1; i <= noOfCol; i++) {
                logSchema.add(rs.getString(i));
            }
        }
        return logSchema;
    }

    private void setupDatabase(List<String> sql) throws SQLException {
        for (String stmt : sql) {
            super.executeStatement(stmt);
            super.getConnection().commit();
        }
    }

    private SampleCollection toBlobs(List<List<String>> samples, List<String> columns, List<String> types) {
        SampleCollection blobs = new SampleCollection();
        for (List<String> row : samples) {
            Sample sample = new Sample(row.get(0));

            for (int i = 1; i < row.size(); i++) {
                if (row.get(i) != null) {
                    sample.addSignal(new Signal(signals.getSignal(columns.get(i)).getId(), types.get(i), row.get(i)));
                }
            }
            blobs.addSample(sample);
        }
        return blobs;
    }

}
