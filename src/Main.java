package datafilteringapp;

import business.ApplicationService;
import business.ApplicationConfigure;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

import persistence.PersistenceBusiness;

/**
 *
 * @author runef
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     * @throws java.sql.SQLException
     * @throws java.lang.ClassNotFoundException
     */
    public static void main(String[] args) throws InterruptedException, SQLException, ClassNotFoundException {

        try {
            ApplicationConfigure appConf = new ApplicationConfigure();
            appConf.configureApp();
//            ApplicationService appService = new ApplicationService();
//            appService.init();
            

            PersistenceBusiness app = PersistenceBusiness.getInstance();
            app.startFiltering();
//            Thread.sleep(10000);
//            app.stopFiltering();
//            if (appConf.getStartWithUi().equals("0")) {
//
//                if (appConf.getFilterFromStart().equals("1")) {
//
//                    appService.filterData();
//                }
//                if (appConf.getFilterContinuously().equals("1")) {
//                    appService.filterContinously();
//                }
//
//            } else if (appConf.getStartWithUi().equals("1")) {
//                ApplicationUI appUi = new ApplicationUI(appService);
//                appUi.start();
//            }
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(ApplicationService.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

    }
}
