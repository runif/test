package persistence;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author runef
 */
public class SampleCollection {
    
    List<Sample> samples = new ArrayList<>();
    
    public SampleCollection() {
        
    }
    
    public void addSample(Sample sample) {
        this.samples.add(sample);
    }
    
    public List<Sample> getSamples() {
        return this.samples;
    }
    
    public void clearSamples() {
        this.samples.clear();
    }
    
}
