package persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import business.TrivialMethods;

/**
 * This is a data access object created specifically for the logger database. It
 * provides access for all the available data on the logger database. It's
 * properties are provided thru an external .properties file.
 *
 * @author runef
 */
public class DAORawSamples extends DbConnection {

    private String fSampleTableName;
    private String fPkName;
    private String fTimestampColName;
    private String fBulkSize;

    public static final String PREFIX = "jdbc:sqlite:";
    public static final String DRIVER_NAME = "org.sqlite.JDBC";

    /**
     *
     * @param url
     * @param sampleTableName
     * @param props
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public DAORawSamples(String url, String sampleTableName, Properties props) throws SQLException, ClassNotFoundException {
        super(DRIVER_NAME, (PREFIX + url), props);

        this.fSampleTableName = sampleTableName;
    }

    /**
     *
     * @param props
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public DAORawSamples(Properties props) throws SQLException, ClassNotFoundException {
        super("org.sqlite.JDBC", ("jdbc:sqlite:" + props.getProperty("url")), props);
        this.fSampleTableName = props.getProperty("SampleTableName");
        this.fPkName = props.getProperty("PkName");
        this.fTimestampColName = props.getProperty("TimeStampColName");
        this.fBulkSize = props.getProperty("BulkSize");
    }

    /**
     * Returns table, 2D array, of the requested samples. The first element of
     * the list contains the column names in the correct order.
     *
     * @param pk
     * @param columns
     * @return
     * @throws SQLException
     */
    public List<List<String>> getSampleBulk(String pk, List<String> columns) throws SQLException {
        String sql = "SELECT " + columns.toString().substring(4, columns.toString().length() - 1)
                + " FROM " + this.fSampleTableName + " WHERE " + this.fPkName + " > " + pk + " LIMIT " + this.fBulkSize;
        ResultSet rs = super.executeQuery(sql);
        return TrivialMethods.resultSetToArray(rs, true, true);
    }

    public List<String> getColumns() throws SQLException {
        List<String> columns;
        String sql = "SELECT * FROM " + this.fSampleTableName + " LIMIT 0";
        ResultSet rs = super.executeQuery(sql);
        columns = TrivialMethods.resultSetToArray(rs, true, false).get(0);
        return columns;
    }

    /**
     * Returns
     *
     * @param pk
     * @param columns
     * @return
     * @throws SQLException
     */
    public ResultSet getSamples(String pk, List<String> columns) throws SQLException {
        String sql = "SELECT " + columns.toString().substring(1, columns.toString().length() - 1)
                + " FROM " + this.fSampleTableName + " WHERE " + this.fPkName + " > "
                + pk + " LIMIT " + this.fBulkSize;
        return super.executeQuery(sql);
    }

    /**
     * Returns the largest timestamp of the signal table
     *
     * @return
     * @throws SQLException
     */
    public String getMaxTimestamp() throws SQLException {
        ResultSet rs = super.executeQuery("SELECT MAX(" + this.fTimestampColName + ") FROM " + this.fSampleTableName);
        return rs.getString("MAX(" + this.fTimestampColName + ")");
    }

    /**
     * Returns the largest primary key value
     *
     * @return - largest primary key value
     * @throws SQLException
     */
    public String getPkMaxValue() throws SQLException {
        String sql = "SELECT MAX(" + this.fPkName + ") FROM " + fSampleTableName;
        ResultSet rs = super.executeQuery(sql);
        return rs.getString("MAX(" + this.fPkName + ")");
    }

    /**
     * Resturns the SQL statements needed to create an identical database
     *
     * @return A result set caontaining SQL statements to create an identical db
     *
     * @throws SQLException if a database access error occurs or this method is
     * called on a closed <code>Statement</code> or the given SQL statement
     * produces anything other than a single <code>ResultSet</code> object
     */
    public ResultSet getDatabaseSchema() throws SQLException {
        return super.executeQuery("SELECT sql FROM sqlite_master");
    }

    
    
}
