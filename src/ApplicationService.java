package business;

import business.ApplicationBusiness;

/**
 * ApplicationService class meant to interact with user. Class handles all
 * communication with the user.
 *
 *
 * @author Rune Fagralon
 * @version v1.0
 */
public class ApplicationService {

    private ApplicationBusiness appBusiness;

    public ApplicationService() {

    }

    public void init() {
        this.appBusiness = ApplicationBusiness.getUniqueInstance();
        this.appBusiness.init();
    }

    public void filterData() {

        this.appBusiness.filterDataFromStart();

    }

    public void filterContinously() {

        this.appBusiness.filterDataContinously();

    }
    
    public void retrieveFilteredSamples() {
        this.appBusiness.retrieveFilterdData();
    }

    public void getFilteredFileSize() {
        throw new UnsupportedOperationException("Under construction");
    }

    public void generateSignalFile() {
        throw new UnsupportedOperationException("Under construction");
    }

    public void terminateApp() {
        
    }
}
