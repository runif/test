package business;

import persistence.SignalCollection;
import persistence.SignalCollection.DbSignal;
import persistence.ApplicationPersistence;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author runef
 */
public class ApplicationBusiness {

    private String signalTableName = "";
    private String maxNoOfRows = "";
    private SignalCollection signals;
    private List<String> colNamesSignalTable = new ArrayList<>();
    
    private boolean firstScan = true;
    private int filterInterval;
    private volatile boolean shouldFilter;
    private static volatile ApplicationBusiness appBusiness;
    private ApplicationPersistence appPers = ApplicationPersistence.getUniqueInstance();

    private ApplicationBusiness() {
    }

    /**
     * Returns a unique instance of this class
     *
     * @return - ApplicationBusiness object
     */
    public static ApplicationBusiness getUniqueInstance() {
        ApplicationBusiness instance = ApplicationBusiness.appBusiness;
        if (instance == null) {
            synchronized (ApplicationBusiness.class) {
                instance = ApplicationBusiness.appBusiness;
                if (ApplicationBusiness.appBusiness == null) {
                    ApplicationBusiness.appBusiness = instance = new ApplicationBusiness();
                }
            }
        }
        return instance;
    }

    // Initializes this app
    public void init() {
        this.signals = SignalCollection.getUniqueInstance();
        try {
            this.setColNamesSignalTable();
        } catch (SQLException ex) {
            Logger.getLogger(ApplicationBusiness.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void filterDataContinously() {
//        DbSignal pkSignal = SignalCollection.getUniqueInstance().getPrimaryKeySignal();
//        if (pkSignal == null) {
//            return;
//        }
//        List<List<String>> rsList;
//
//        pkSignal.setValue(appPers.getLargestPkRawSamples());
//        while (true) {
//            rsList = appPers.getRawSamples(pkSignal.getValue(), colNamesSignalTable);
//            List<String> colNames = rsList.remove(0);
//            rsList = this.checkForNewVal(rsList, colNamesSignalTable);
//
//            if (!rsList.isEmpty()) {
//
//                appPers.insertFilteredSamples(rsList, colNames);
//
//            }
//        }

    }
    
    private Long lastRetrieved = 0L;
    public void filterDataFromStart() {
//        List<List<String>> rsList;
//
//        boolean notFinished = true;
//
//        while (notFinished) {
//            // Select all rows from the logger where the primary key is
//            // greater than the last one written to the filtered database
//
//            rsList = appPers.getRawSamples(lastRetrieved.toString(), colNamesSignalTable);
//
//            List<String> colNames = rsList.remove(0);
//            lastRetrieved += rsList.size();
//
//            // Check if any values were retrieved
//            notFinished = !rsList.isEmpty();
//
//            // Check if the list contains any data
//            if (notFinished) {
//
//                // Check for new values and update result set list
//                rsList = this.checkForNewVal(rsList, colNames);
//                // Check if the DB file has reached it's max size. If so, start updating old data
//
//                // If the DB file hasn't reached it's max size, keep inserting new rows
//                appPers.insertFilteredSamples(rsList, colNames);
//
//            }
//        }
    }

    public void retrieveFilterdData() {
        
        List<String> col = new ArrayList<>();
        col.add("timestamp");
        col.add("CurrSystemState");
        col.add("SlewPosition");

        List<List<String>> samples = appPers.getFilteredSample("1454538056.90162", col);
        for (List<String> sample : samples) {
            for (String signal : sample) {
                System.out.print(signal + " ");
            }
            System.out.print("\n");
        }

    }

    /**
     * Gracefully terminates this app
     */
    public void terminateApp() throws SQLException {

    }

    /*
    Writes all column names to a list in the same order as in the DB table
     */
    private void setColNamesSignalTable() throws SQLException {
        List<DbSignal> sig = signals.getSignals();
        for (DbSignal item : sig) {
            this.colNamesSignalTable.add(item.getName());
        }
    }

    /*
     * Method if the values passed to it are new values compared to the last value
     * written to a signal.
     */
    private List<List<String>> checkForNewVal(List<List<String>> rsList, List<String> colNames) {
        DbSignal sig;
        String currVal;
        boolean triggerFound;
        int colNo;
        List<String> row;
        String col;
        ListIterator<List<String>> itRow = rsList.listIterator();
        while (itRow.hasNext()) {
            row = itRow.next();
            ListIterator<String> itCol = row.listIterator();
            triggerFound = false;
            while (!triggerFound && itCol.hasNext()) {
                colNo = itCol.nextIndex();
                col = itCol.next();
                sig = this.signals.getSignal(colNames.get(colNo));
                if (sig.isTrigger(col)) {
                    triggerFound = true;
                }
            }
            if (!triggerFound) {
                itRow.remove();
            }
        }
        // If the result list is not empty, filter the remaining data
        // The first row will alwyays be kept with all values
        if (!rsList.isEmpty()) {
            for (List<String> element : rsList) {
                for (int i = 0; i < element.size(); i++) {
                    // The first row of the result will not be filtered, 
                    // i.e. all values will be written to the filtered database
                    sig = signals.getSignal(colNames.get(i));
                    currVal = element.get(i);

                    // If this is the first filtering cycle since app start, let
                    // all values pass thru un-filtered
                    if (firstScan) {
                        sig.setValue(currVal);

                        // Check if the current value is a new value. If it is a 
                        // new value, update the signal object and leave the value 
                        // unchanged in the table. If not new value, set the table 
                        // value to null
                    } else if (sig.hasNewValue(currVal)) {
                        sig.setValue(currVal);
                    } else {
                        element.set(i, null);
                    }

                }
                if (firstScan == true) {
                    firstScan = false;
                }
            }
        }
        return rsList;
    }

    /**
     * @return the signalTableName
     */
    public String getSignalTableName() {
        return signalTableName;
    }

    /**
     * @param signalTableName the signalTableName to set
     */
    public void setSignalTableName(String signalTableName) {
        this.signalTableName = signalTableName;
    }

    /**
     * @return the maxNoOfRows
     */
    public String getMaxNoOfRows() {
        return maxNoOfRows;
    }

    /**
     * @param maxNoOfRows the maxNoOfRows to set
     */
    public void setMaxNoOfRows(String maxNoOfRows) {
        this.maxNoOfRows = maxNoOfRows;
    }

    /**
     * @return the filterInterval
     */
    public int getFilterInterval() {
        return filterInterval;
    }

    /**
     * @param filterInterval the filterInterval to set
     */
    public void setFilterInterval(int filterInterval) {
        this.filterInterval = filterInterval;
    }

    /**
     * @return the shouldFilter
     */
    public boolean isShouldFilter() {
        return shouldFilter;
    }

    /**
     * @param shouldFilter the shouldFilter to set
     */
    public void setShouldFilter(boolean shouldFilter) {
        this.shouldFilter = shouldFilter;
    }

}
