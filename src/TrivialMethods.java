package business;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is not meant to be instantiated. It will only take a
 * {@code ResultSet} and return a list of arrays of type string.
 *
 * @author runef
 */
public class TrivialMethods {

    private TrivialMethods() {
    }

    /**
     * This method will return a List of arrays of type String. Item no. 0 will
     * contain the names of the columns and item no. 1 will contain its data
     * types. The rest of the list will contain the columns values.{@literal
     * | item 0 | name | name | ... |
     * |--------|------|------|-----|
     * | item 1 | type | type | ... |
     * |--------|------|------|-----|
     * | item 2 | val  | val  | ... |
     * |--------|------|------|-----|
     * }
     * If {@code withMetaData} is set false, the List will only have the value
     * elements.
     *
     * @param rs A database result set
     * @param withColumnNames Boolean value if the result should be with name
     * and type.
     * @return A list of string arrays
     * @throws SQLException - if the passed {@code ResultSet} is closed or a
     * database access error occurs.
     */
    public static List<List<String>> resultSetToArray(ResultSet rs,
            boolean withColumnNames, boolean withDataTypes) throws SQLException {

        ResultSetMetaData rsmd = rs.getMetaData();
        int noOfColumns = rsmd.getColumnCount();
        List<List<String>> rsArray = new ArrayList<>();
        List<String> columnNames = new ArrayList<>();
        List<String> columnDataTypes = new ArrayList<>();

        if (withColumnNames) {
            // Add the column names as the header or rather the first row of this array
            for (int i = 0; i < noOfColumns; i++) {
                columnNames.add(i, rsmd.getColumnName(i + 1));
            } // for
            rsArray.add(columnNames);
        }
        // Add the column data types as the second row
        if (withDataTypes) {
            for (int i = 0; i < noOfColumns; i++) {
                columnDataTypes.add(i, (rsmd.getColumnTypeName(i + 1)));
            } // for
            rsArray.add(columnDataTypes);
        }

        while (rs.next()) {
            List<String> columnValues = new ArrayList<>();
            for (int i = 0; i < noOfColumns; i++) {
                columnValues.add(i, rs.getString(i + 1));
            } // for

            rsArray.add(columnValues);

        } // while
        return rsArray;
    } // resultSetToArray

    /**
     * Iterates a table and finds the last valid value written to the table. If
     * the table below is input {@literal
     * |--------|------|------|
     * |    1   | null | null |
     * |--------|------|------|
     * |  null  |   2  | null |
     * |--------|------|------|
     * |  null  | null |   3  |
     * |--------|------|------|
     * }
     * This method will return the following row {@literal
     * |--------|------|------|
     * |    1   |   2  |   3  |
     * |--------|------|------|
     * }
     *
     * @param values - tabele to be analyzed
     * @return - a row from input table with the latest valid values written to
     * the table
     */
    public static List<String> getLastValidRow(List<List<String>> values) {
        List<String> retList = new ArrayList<>(values.get(0));
        
        for (int i = 1; i < values.size(); i++) {
            List<String> item = values.get(i);
            for (int j = 0; j < item.size(); j++) {
                if (item.get(j) != null) {
                    if (!item.get(j).equals(retList.get(j))) {
                        retList.set(j, item.get(j));
                    } // if
                } // if
            } // for
        } // for
        return retList;
    } // CheckValues

    /**
     * Iterates a table and finds the last valid value written to the table. If
     * the table below is input {@literal
     * |--------|------|------|
     * |    1   |   2  |   3  |
     * |--------|------|------|
     * |  null  | null | null |
     * |--------|------|------|
     * |  null  | null | null |
     * |--------|------|------|
     * }
     * This method will return the following row {@literal
     * |--------|------|------|
     * |    1   |   2  |   3  |
     * |--------|------|------|
     * |    1   |   2  |   3  |
     * |--------|------|------|
     * |    1   |   2  |   3  |
     * |--------|------|------|
     * }
     *
     * @param table
     * @return
     */
    public static List<List<String>> populateTable(List<List<String>> table) {

        List<String> validRow = new ArrayList<>(table.get(1));

        for (int i = 2; i < table.size(); i++) {
            for (int j = 0; j < table.get(i).size(); j++) {
                if (table.get(i).get(j) == null) {
                    table.get(i).set(j, validRow.get(j));
                } else if (table.get(i).get(j) != null) {
                    validRow.set(j, table.get(i).get(j));
                }
            }
        }

        return table;
    }

}
