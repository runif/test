package business;

import persistence.SignalCollection;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author runef
 */
public class XmlFileReader {

    private String fileUrl;
    private DocumentBuilderFactory docBuilderFac;
    private Document document;
    private DocumentBuilder docBuilder;

    public XmlFileReader(String URI) throws ParserConfigurationException, SAXException, IOException {
        this.fileUrl = URI;
        docBuilderFac = DocumentBuilderFactory.newInstance();
        docBuilder = docBuilderFac.newDocumentBuilder();
        document = docBuilder.parse(new File(fileUrl));
        document.getDocumentElement().normalize();
    }

    /**
     * Returns a list containing all signal parameters in order
     * name, type, initiVal, sensitivity, primaryKey
     * @return - List containing signal parameters
     */
    public List<List<String>> getSignalParams() {
        NodeList signalGroupNode = document.getElementsByTagName("Signals");
        List<List<String>> retList = new ArrayList<>();
        for (int i = 0; i < signalGroupNode.getLength(); i++) {
            Element sigGroupElement = (Element) signalGroupNode.item(i);

            NodeList signalNodes = sigGroupElement.getElementsByTagName("Signal");
            for (int j = 0; j < signalNodes.getLength(); j++) {
                List<String> param = new ArrayList<>();
                Element signalElement = (Element) signalNodes.item(j);
                
                String name = signalElement.getAttribute("name");
                param.add(name);
                String dataType = signalElement.getAttribute("type");
                param.add(dataType);
                String value = signalElement.getAttribute("initVal");
                param.add(value);
                String sens = signalElement.getAttribute("sensitivity");
                param.add(sens);
                String pk = signalElement.getAttribute("primaryKey");
                param.add(pk);
                String triggers = signalElement.getAttribute("triggers");
                param.add(triggers);
                retList.add(param);
                String id = signalElement.getAttribute("id");
                param.add(id);
            }
        }
        return retList;
    }

    /**
     * Return the URL of the filtered database
     *
     * @return - URL of filtered DB
     */
    public String getUrlFiltered() {
        String url = "";
        NodeList elements = this.document.getElementsByTagName("DbFiltered");
        if (elements.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) elements.item(0);
            url = element.getAttribute("url");
        }
        return url;
    }

    public String getSignalTableName() {
        String tableName = "";
        NodeList elements = this.document.getElementsByTagName("DbLogger");
        if (elements.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) elements.item(0);
            tableName = element.getAttribute("signalTableName");
        }
        return tableName;
    }

    public String getMaxNoOfRows() {
    String noOfRows = "";
        NodeList elements = this.document.getElementsByTagName("MaxRowsPerQuery");
        if (elements.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) elements.item(0);
            noOfRows = element.getAttribute("noOfRows");
        }
        return noOfRows;
    }

    /**
     * Return the URL of the logger database
     *
     * @return - URL of logger DB
     */
    public String getUrlLogger() {
        String url = "";
        NodeList elements = this.document.getElementsByTagName("DbLogger");
        if (elements.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) elements.item(0);
            url = element.getAttribute("url");
        }
        return url;
    }
    
    /**
     * Get the maximum allowed file size of the filtered database
     * @return - max file size
     */
    public String getMaxFileSizeFiltered() {
        String size = "";
        NodeList elements = this.document.getElementsByTagName("DbFiltered");
        if (elements.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) elements.item(0);
            size = element.getAttribute("maxFileSize");
        }
        return size;
    }
    
    public String getFilterInterval() {
        String filterInterval = "";
        NodeList elements = this.document.getElementsByTagName("App");
        if (elements.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) elements.item(0);
            filterInterval = element.getAttribute("filterInterval");
        }
        return filterInterval;
    }
    
    public String getFilterFromStart() {
        String filterFromStart = "";
        NodeList elements = this.document.getElementsByTagName("App");
        if (elements.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) elements.item(0);
            filterFromStart = element.getAttribute("filterFromStart");
        }
        return filterFromStart;
    }
    
    public String getStartWithUi() {
        String startWithUi = "";
        NodeList elements = this.document.getElementsByTagName("App");
        if (elements.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) elements.item(0);
            startWithUi = element.getAttribute("startWithUi");
        }
        return startWithUi;
    }
    
    public String getFilterContinuously() {
        String filterCont = "";
        NodeList elements = this.document.getElementsByTagName("App");
        if (elements.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) elements.item(0);
            filterCont = element.getAttribute("filterContinuously");
        }
        return filterCont;
    }

}
